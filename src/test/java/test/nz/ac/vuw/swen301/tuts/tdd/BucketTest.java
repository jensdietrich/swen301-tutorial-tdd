package test.nz.ac.vuw.swen301.tuts.tdd;

import org.junit.*;
import static org.junit.Assert.*;
import nz.ac.vuw.swen301.tuts.tdd.Bucket;
/**
 * Tests. 
 * DO NOT ALTER THOSE TESTS, THIS IS THE SPECIFICATION YOU ARE IMPLEMENTING.
 */
public class BucketTest {

	private Bucket bucket = null;

	@Before
	public void setup() {
		this.bucket = new Bucket();
	}

	@After
	public void teardown() {
		this.bucket = null;
	}

    @Test
    public void testAddOne () {
        boolean ok = bucket.add("foo");
        assertEquals(1,bucket.getSize());
		assertTrue(ok);
    }

	@Test
	public void testAddTwo () {
		boolean ok1 = bucket.add("one");
		boolean ok2 = bucket.add("two");
		assertEquals(2,bucket.getSize());
		assertTrue(ok1);
		assertTrue(ok2);
	}

	@Test
	public void testAddOneTwice1 () {
		String s1 = "one";
		String s2 = "one";

		// asserts are mainly for documentation. copuld use assumptions instead (=junit preconditions)
		assert s1==s2;
		assert s1.equals(s2);

		boolean ok1 = bucket.add(s1);
		boolean ok2 = bucket.add(s2);
		assertEquals(1,bucket.getSize());
		assertTrue(ok1);
		assertFalse(ok2);
	}

	@Test
	public void testAddOneTwice2 () {
		String s1 = "one";
		String s2 = new String("one");

		// asserts are mainly for documentation. copuld use assumptions instead (=junit preconditions)
		assert s1!=s2;
		assert s1.equals(s2);

		boolean ok1 = bucket.add(s1);
		boolean ok2 = bucket.add(s2);
		assertEquals(1,bucket.getSize());
		assertTrue(ok1);
		assertFalse(ok2);
	}

	@Test
	public void testRemoveOne () {
		String s = "one";

		boolean ok1 = bucket.add(s);
		boolean ok2 = bucket.remove(s);

		assertTrue(ok2);
		assertEquals(0,bucket.getSize());
	}

	@Test
	public void testRemoveOther () {
		String s1 = "one";
		String s2 = "two";

		bucket.add(s1);
		boolean ok = bucket.remove(s2);

		assertFalse(ok);
		assertEquals(1,bucket.getSize());
	}

	@Test
	public void testAddMany () {
		int COUNT = 10000;
		for (int i=0;i<COUNT;i++) {
			bucket.add("element-"+i);
		}
		assertEquals(COUNT,bucket.getSize());
	}


}
